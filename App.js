import React from 'react';
import { StyleSheet, TouchableHighlight, Text, Alert, View, FlatList, TextInput } from 'react-native';
import axios from 'axios'
import { Video } from 'expo'
import DismissKeyboard from 'dismissKeyboard';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#4f5cc4',
    alignItems: 'center',
    justifyContent: 'flex-start',
    color: 'white',
  },
  wrapper:{
    marginTop: 30,
  },
  backgroundVideo: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
  button: {
    width: 260,
    alignItems: 'center',
    backgroundColor: '#db4060',
    borderRadius: 30
  },
  buttonText: {
    padding: 20,
    color: 'white'
  },
  buttonHighlight: {
    borderRadius: 30
  },
  title: {
    alignSelf: 'center',
    textAlign: 'center',
    color: 'white',
    padding: 15,
    textShadowColor: 'black',
    textShadowRadius: 20,
    textShadowOffset: {width: -1, height: 1}
  }
});


export default class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      links: null,
      isHovering: false,
      modalVisible: false,
      searchString: '',
    }
    this.setModalVisible = this.setModalVisible.bind(this)
  }
  componentDidMount() {

  }
  getLinks() {
    let links = []
    let searchString = this.state.searchString.replace(/(\/)?r\//, '')
    axios.get(`https://www.reddit.com/r/${searchString}.json?limit=100`)
      .then(response => {
        let posts = response.data.data.children
        for (let i = 0; i < posts.length; i++) {
          if (posts[i].data.secure_media_embed !== null || posts[i].data.media !== null || posts[i].data.secure_media !== null) {
            if (posts[i].data.preview) {
              if (posts[i].data.preview.reddit_video_preview !== null && posts[i].data.preview.reddit_video_preview !== undefined) {
                links.push({ id: i, link: posts[i].data.preview.reddit_video_preview.hls_url })
                continue
              }
            }
            if (posts[i].data.media){
              if (posts[i].data.media.reddit_video !== null && posts[i].data.media.reddit_video !== undefined) {
                links.push({ id: i, link: posts[i].data.media.reddit_video.hls_url })
                continue
              }
            }
            if (posts[i].data.secure_media){
              if (posts[i].data.secure_media.reddit_video !== null && posts[i].data.secure_media.reddit_video !== undefined) {
                links.push({ id: i, link: posts[i].data.secure_media.reddit_video.hls_url })
                continue
              }
            }
          }
        }
        this.setState({
          links: links,
          errors: null
        })
        this.forceUpdate();
      })
      .catch(error => {
        this.setState({
          errors: error.response.status
        })
        this.forceUpdate();
      })
  }

  setModalVisible() {
    this.setState({ modalVisible: !this.state.modalVisible });
  }

  render() {
    let videoList
    if (this.state.links !== null && this.state.links !== [] && !this.state.errors) {
      videoList = <FlatList style={{maxHeight: '90%'}}
        data={this.state.links}
        renderItem={({ item }) => <Video
          source={{ uri: item.link }}
          rate={1.0}
          volume={1.0}
          isMuted={false}
          resizeMode="contain"
          shouldPlay={false}
          useNativeControls
          isLooping
          style={{ width: 350, height: 250 }}
          containerStyle={{ borderTopWidth: 0, borderBottomWidth: 0 }}
        />}
        keyExtractor={item => item.link}
        ItemSeparatorComponent={() => {
          return (
            <View
              style={{
                height: 10,
                width: "86%",
                marginLeft: "14%"
              }}
            />
          );
        }}
      />
    } else if(this.state.errors){

      videoList = <Text style={{color: 'white'}}>{this.state.errors}</Text>
    } else { videoList = <Text style={{color: 'white'}}>Nothing to load</Text> }

    return (
      <View className="App" style={styles.container} >
        <View style={styles.wrapper}>
          <Text className="App-title" style={styles.title}>Welcome to <Text style={{color: '#db4060'}}>Reddit</Text></Text>
          <TextInput
                  style={{ height: 40, borderColor: 'gray', borderWidth: 1, backgroundColor: 'white', textAlign: 'center', marginBottom: 5 }}
                  onSubmitEditing={() => this.getLinks()}
                  onChangeText={(text) => this.setState({ searchString: text })}
                  placeholder="/r/<Type the subreddit here>"
                />
          <TouchableHighlight 
            style={styles.buttonHighlight} 
            onPress={()=>{
              this.getLinks()
              DismissKeyboard()
            }} 
            underlayColor="purple">
            <View style={styles.button}>
              <Text style={styles.buttonText}>Change Subreddit</Text>
            </View>
          </TouchableHighlight>

        </View>

        

        <View style={{ marginTop: "2%", paddingBottom: '5%' }} containerStyle={{ borderTopWidth: 0, borderBottomWidth: 0 }}>
          {videoList}
        </View>


      </View>
    );
  }
}
